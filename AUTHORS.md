# Authors 

* Cristian Martinez <martinec> (Cogniteva SAS)
* Lionel Villard    <lvillard> (CorTexT)
* Philippe Breucker <breuker>  (CorTexT)

# Contributors

* Alexandre Abdo    <abdo> (CorTexT)
