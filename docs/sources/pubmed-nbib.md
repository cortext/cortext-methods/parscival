# PubMed Nbib Processing

## Parscival specification

```eval_rst
.. literalinclude:: ../../src/parscival_specs/pubmed/pubmed-nbib.yaml
   :language: yaml
```
